package com.example.springbootmybatiscrudexample.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class News {
    private int id;
    private String img;
    private String content;

    public News(String img, String content) {
        this.img = img;
        this.content = content;
    }
}
