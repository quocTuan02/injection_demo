package com.example.springbootmybatiscrudexample.controller;


import com.example.springbootmybatiscrudexample.model.News;
import com.example.springbootmybatiscrudexample.repository.NewsRepository;
import com.example.springbootmybatiscrudexample.response.SuccessResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
public class NewsController {
    private final NewsRepository newsRepository;

    @PostMapping("/news")
    public ResponseEntity<SuccessResponse<News>> add(@RequestBody News news) {
        var insert = newsRepository.insert(news);
        return ResponseEntity.ok(new SuccessResponse<>(HttpStatus.OK.value(), null, news));
    }

    @GetMapping("/news/{id}")
    public ResponseEntity<SuccessResponse<?>> findById(@PathVariable String id) {
        try {
            var news = newsRepository.findById(id);
            return ResponseEntity.ok(new SuccessResponse<>(HttpStatus.OK.value(), null, news));
        } catch (Exception e) {
            return ResponseEntity.ok(new SuccessResponse<>());
        }
    }
}
