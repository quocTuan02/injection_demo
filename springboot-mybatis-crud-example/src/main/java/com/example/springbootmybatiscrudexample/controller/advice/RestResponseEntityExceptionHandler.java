package com.example.springbootmybatiscrudexample.controller.advice;


import com.example.springbootmybatiscrudexample.response.ErrorsResponse;
import com.example.springbootmybatiscrudexample.response.data.MethodArgumentNotValidResponse;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public RestResponseEntityExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }



    @ExceptionHandler(value = {
            NotFoundException.class,
            HttpRequestMethodNotSupportedException.class,
            HttpMediaTypeNotSupportedException.class
    })
    protected ResponseEntity<ErrorsResponse<Object>> handleNotFound(Exception notFoundException) {
        String errorMessage;
        try {
            errorMessage = messageSource.getMessage(notFoundException.getMessage().replaceAll("[{}]", ""), null, LocaleContextHolder.getLocale());
        } catch (Exception exception) {
            errorMessage = notFoundException.getMessage();
        }

        var err = new ErrorsResponse<>(HttpStatus.BAD_REQUEST.value(), errorMessage, null);
        return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BindException.class})
    protected ResponseEntity<ErrorsResponse<List<MethodArgumentNotValidResponse>>> handleBindException(BindException ex) {
        String message = messageSource.getMessage("bad-request", null, LocaleContextHolder.getLocale());

        List<MethodArgumentNotValidResponse> errors = ex.getFieldErrors().stream().map(e ->
                new MethodArgumentNotValidResponse(e.getField(), e.getDefaultMessage())
        ).collect(Collectors.toList());

        ErrorsResponse<List<MethodArgumentNotValidResponse>> response = new ErrorsResponse<>(HttpStatus.BAD_REQUEST.value(), message, errors);
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AuthenticationException.class})
    protected ResponseEntity<ErrorsResponse<Object>> handleAuthenticationException(AuthenticationException ex) {
        ErrorsResponse<Object> response = new ErrorsResponse<>(HttpStatus.UNAUTHORIZED.value(), ex.getMessage(), null);
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }


    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    protected ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException exception) {
        var message = "bad-request";

        if (exception.getCause() instanceof JsonMappingException) {
            var jsonMappingException = ((JsonMappingException) exception.getCause());
            String errorMessage = "";
            var fieldName = new StringBuilder();
            for (var path : jsonMappingException.getPath()) {
                if (path.getIndex() == -1) {
                    if (path.getFieldName() == null) {
                        continue;
                    }
                    if (fieldName.length() == 0) {
                        fieldName.append(path.getFieldName());
                    } else {
                        fieldName.append(".").append(path.getFieldName());
                    }
                } else {
                    fieldName.append("[").append(path.getIndex()).append("]");
                }
            }
            String errorCodeMessage = exception.getLocalizedMessage();
             if (jsonMappingException instanceof MismatchedInputException) {
                var mismatchedInputException = ((MismatchedInputException) jsonMappingException);
                switch (mismatchedInputException.getTargetType().getName()) {
                    case "java.time.Instant":
                        errorCodeMessage = "time.invalid";
                        break;
                    case "java.lang.Long":
                    case "java.lang.Integer":
                        errorCodeMessage = "number.invalid";
                        break;
                    case "java.util.ArrayList":
                        errorCodeMessage = "list.invalid";
                        break;
                    default:{
                        log.warn(exception.getMessage(), exception);
                        errorCodeMessage = "object.invalid";
                    }
                }
            }
            try {
                errorMessage = messageSource.getMessage(errorCodeMessage, null, LocaleContextHolder.getLocale());
            } catch (Exception ex) {
                errorMessage = errorCodeMessage;
            }
            var response = new ErrorsResponse<>(HttpStatus.BAD_REQUEST.value(), message, List.of(new MethodArgumentNotValidResponse(fieldName.toString(), errorMessage)));

            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
        log.error(exception.getMessage(), exception);
        ErrorsResponse<Object> response = new ErrorsResponse<>(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.name(),
                null
        );
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {HttpClientErrorException.class})
    protected ResponseEntity<?> handleHttpClientErrorException(HttpClientErrorException exception) {
        var response = new ErrorsResponse<>(
                exception.getStatusCode().value(),
                exception.getStatusCode().name(),
                null
        );
        return new ResponseEntity<>(response, new HttpHeaders(), exception.getStatusCode());
    }

    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    protected ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
        var message = messageSource.getMessage("bad-request", null, LocaleContextHolder.getLocale());
        String errorCodeMessage;

        var requiredType = exception.getRequiredType();
        if (requiredType != null) {
            switch (requiredType.getName()) {
                case "java.time.Instant":
                    errorCodeMessage = "time.invalid";
                    break;
                case "java.lang.Long":
                case "java.lang.Integer":
                    errorCodeMessage = "number.invalid";
                    break;
                default:
                    errorCodeMessage = exception.getLocalizedMessage();
            }
        } else {
            errorCodeMessage = exception.getLocalizedMessage();
        }

        ErrorsResponse<List<MethodArgumentNotValidResponse>> response;
        try {
            String errorMessage = messageSource.getMessage(errorCodeMessage, null, LocaleContextHolder.getLocale());
            response = new ErrorsResponse<>(HttpStatus.BAD_REQUEST.value(), message, List.of(new MethodArgumentNotValidResponse(exception.getName(), errorMessage)));
        } catch (Exception ex) {
            response = new ErrorsResponse<>(HttpStatus.BAD_REQUEST.value(), message, List.of(new MethodArgumentNotValidResponse(exception.getName(), errorCodeMessage)));
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    // Xử lý tất cả các exception chưa được khai báo
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handlerException(Exception ex) {
        log.error(ex.getMessage(), ex);
        ErrorsResponse<Object> response = new ErrorsResponse<>(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                HttpStatus.INTERNAL_SERVER_ERROR.name(),
                null
        );
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
