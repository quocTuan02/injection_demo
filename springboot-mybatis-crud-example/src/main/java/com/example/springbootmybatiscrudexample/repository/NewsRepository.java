package com.example.springbootmybatiscrudexample.repository;

import com.example.springbootmybatiscrudexample.model.News;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import java.util.List;

@Mapper
public interface NewsRepository {
    @Insert("INSERT INTO news(img, content)  VALUES (#{img}, #{content})")
    @SelectKey(keyProperty = "id", before = false, statement = "SELECT LAST_INSERT_ID()", resultType = Integer.class)
    int insert(News news);

    @Select("SELECT * from news WHERE id = ${id}")
    List<News> findById(String id);

    @Select("select * from news")
    List <News> findAll();
}
