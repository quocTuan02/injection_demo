package com.example.springbootmybatiscrudexample.response;


import lombok.Data;

@Data
public class UserResponse {
    private String userId;
    private String userName;
    private String contractId;
    private String userType;
    private String industryType;
    private String companyName;
}
